alert("hello world");

let trainers = {
	name:'James',
	gender:'Male',
	action: function(){
		talk();
	}
}

console.log(trainers);
console.log(`
	Trainor Name: ${trainers.name} \n 
	Gender: ${trainers['gender']}
`);

let pokemon1 = new Pokemon('Charmander');
let pokemon2 = new Pokemon('Balbausar');
console.log(pokemon1.action(pokemon2.name));
console.log(pokemon2.action(pokemon1['name']));

function Pokemon(name,action){
	this.name = name,
	this.action = function(target){
		console.log(`${this.name} use Tackle on ${target}`);
	}
}


function talk(){
	alert("Hello World");
}